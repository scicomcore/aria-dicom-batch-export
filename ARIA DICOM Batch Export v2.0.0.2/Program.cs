﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using EvilDICOM.Network;
using EvilDICOM.Core.Helpers;
using EvilDICOM.Network.DIMSE;
using EvilDICOM.Network.SCUOps;
using VMS.TPS.Common.Model.API;
using System.Collections.Generic;
using EvilDICOM.Network.DIMSE.IOD;


[assembly: AssemblyVersion("2.0.0.2")]
[assembly: AssemblyFileVersion("2.0.0.2")]

namespace DICOMExportForPROKNOW
{
	class Program
	{
		//=== USER CONFIGURATION ===============================================================================================================================================

		// Create variables to set client details later in Main function
		private static string clientTitle;
		private static int clientPort;

		// User required to set following variables for local DICOM daemon (DB service) settings
		private static string daemonTitle = "";						// e.g. "ProKnowDB"
		private static string daemonIP = "";						// e.g. "194.155.20.205"
		private static int daemonPort = ;							// e.g. 51402

		// User required to set default input data file path (inpath) & ProKnow DICOM Agent directory (outpath) as well as export log path directory (logpath). When script is run, inpath (and outpath if required) can be modified in the GUI prior to starting export 
		private static string inpath = "";							// e.g. "S:\\PHYSICS"
		private static string outpath = "";							// e.g. "\\\\194.155.20.90\\ProknowFiles"
		private static string logPath = "";							// e.g. "\\\\auraserver\\ariashare$\\PHYSICS\\ProKnow\\Export Log Files";

		//====================================================================================================================================================================

		// Lists created to store patient, course and plan data unpacked from data input file
		private static List<string> patIDs = new List<string>();
		private static List<string> courseIDs = new List<string>();
		private static List<string> planIDs = new List<string>();

		//====================================================================================================================================================================

		[STAThread]
		private static void Main(string[] args)
		{
			bool clientIdentified = false;

			//Client configuration
			if (Environment.MachineName.Contains(""))
			{
				clientTitle = "";
				clientPort = ;
				clientIdentified = true;

			}
			else if (Environment.MachineName.Contains(""))
			{
				clientTitle = "";
				clientPort = ;
				clientIdentified = true;
			}
			else if (Environment.MachineName.Contains("")) 
			{
				clientTitle = "";
				clientPort = ;
				clientIdentified = true;

			} else if (Environment.MachineName.Contains(""))
            {
				clientTitle = "";
				clientPort = ;
				clientIdentified = true;
			}
			else if (Environment.MachineName.Contains(""))
			{
				clientTitle = "";
				clientPort = ;
				clientIdentified = true;
			}
			else
            {
				Console.WriteLine("ERROR: Could not identify client settings for " + Environment.MachineName + ". Program terminated");
            }

			if (clientIdentified)
			{
				try
				{
					Console.Title = "DICOM Export for ProKnow";
					Console.ForegroundColor = ConsoleColor.White;
					Console.WriteLine(" ");
					Console.WriteLine(" ");
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.WriteLine("----------------------ARIA: DICOM Batch Export for ProKnow---------------------");
					Console.WriteLine("----------------------------------- v2.0.0.2 ----------------------------------");
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.WriteLine(" ");
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("----------------------------------PLEASE READ----------------------------------");
					Console.ForegroundColor = ConsoleColor.White;
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.WriteLine("--- This is a DICOM export program designed to batch export DICOM files from --");
					Console.WriteLine("---- ARIA based on details from a data input file containing patient IDs,  ----");
					Console.WriteLine("--------------------------- course IDs and plan IDs. --------------------------");
					Console.WriteLine("");
					Console.WriteLine("----- This script was developed at the Royal Surrey County Hospital using -----");
					Console.WriteLine("------------------------ ARIA v15.1 and Eclipse v15.6 -------------------------");
					Console.WriteLine(" ");
					Console.WriteLine("-- The authors take no responisbility when this program is run at a different -");
					Console.WriteLine("-- centre. Please read fully the associated documentation prior to installing -");
					Console.WriteLine("-- and testing the program. It is strongly recommended that this program is  --");
					Console.WriteLine("------ tested on the TBOX prior to being run on the clincal ARIA system.-------");
					Console.WriteLine("-------------------------------------------------------------------------------");
					Console.WriteLine(" ");



					using (VMS.TPS.Common.Model.API.Application app = VMS.TPS.Common.Model.API.Application.CreateApplication())
					{
						Execute(app);
					}
				}
				catch (Exception exception)
				{
					Console.WriteLine("Exception was thrown: " + exception.Message);
					Console.ReadKey();
				}
			}
		}

		//====================================================================================================================================================================

		private static void Execute(VMS.TPS.Common.Model.API.Application app)
		{
			// While loop to allow multiple export sessions whilst program is running
			while (true)
			{
				// Read in data from GUI and data input file
				using (Form1 form = new Form1(app, daemonTitle, daemonIP, daemonPort, clientTitle, clientPort, inpath, outpath))
				{
					form.ShowDialog();
					patIDs = form.patientIDs;
					courseIDs = form.courseIDs;
					planIDs = form.planIDs;
					daemonTitle = form.daemonTitle;
					daemonIP = form.daemonIP;
					daemonPort = form.daemonPort;
					clientTitle = form.clientTitle;
					clientPort = form.clientPort;
					inpath = form.inpath;
					outpath = form.outpath;
				}

				if ((patIDs.Count == courseIDs.Count) && (patIDs.Count == planIDs.Count) && patIDs.Count > 0)
				{
					// Create lists to store DICOM UID data as these are required by the EvilDICOM export function
					List<string> planUIDs = new List<string>() { };
					List<string> structUIDs = new List<string>() { };
					List<string> doseUIDs = new List<string>() { };
					List<string> ctUIDs = new List<string>() { };

					// List to store any patient data from the input file where patient/course/plan could not be identified in ARIA
					List<string> missing_data_log = new List<string>() { };

					// List of patient IDs whose plan data could be found in ARIA
					List<string> verified_patID = new List<string>() { };

					// Loop through # patients in input file
					for (int i = 0; i < patIDs.Count(); i++)
					{
						Patient pat = null;
						bool patient_found = false;
						bool course_found = false;
						bool plan_found = false;
						bool dose_valid = false;

						// Attempt to open patient and identify course and plan for each line of the data input file
						try
						{
							pat = app.OpenPatientById(patIDs[i]);

							if (pat != null)
							{
								patient_found = true;
								foreach (Course course in pat.Courses.ToList())
								{
									if (course.Id == courseIDs[i])
									{
										course_found = true;
										foreach (PlanSetup plan in course.PlanSetups.ToList())
										{
											if (plan.Id == planIDs[i])
											{
												plan_found = true;
												if (plan.IsDoseValid)
												{
													// If patient, course and plan are identified and plan has dose, add patient ID to verified_patID list and add DICOM UIDs to relevant lists 
													dose_valid = true;
													verified_patID.Add(pat.Id);
													planUIDs.Add(plan.UID);
													structUIDs.Add(plan.StructureSet.UID);
													doseUIDs.Add(plan.Dose.UID);
													ctUIDs.Add(plan.StructureSet.Image.Series.UID);
												}
											}
										}
									}
								}
							}
							app.ClosePatient();
							if (!dose_valid)
							{
								missing_data_log.Add(patIDs[i] + " (course: " + courseIDs[i] + ", plan: " + planIDs[i] + "): Patient found (" + patient_found + "), Course found (" + course_found + "), Plan found (" + plan_found + "), Dose valid (" + dose_valid + ")");
							}
						}
						catch
						{
							missing_data_log.Add(patIDs[i] + " (course: " + courseIDs[i] + ", plan: " + planIDs[i] + "): Patient found (" + patient_found + "), Course found (" + course_found + "), Plan found (" + plan_found + "), Dose valid (" + dose_valid + ")");
						}
					}

					// Establish DICOM daemon and client
					Entity daemon = new Entity(daemonTitle, daemonIP, daemonPort);
					Entity local = Entity.CreateLocal(clientTitle, clientPort);
					DICOMSCU client = new DICOMSCU(local);

					// Set up receiver to catch files as they come in
					var receiver = new DICOMSCP(local);

					// Let the daemon know we can take anything it sends
					receiver.SupportedAbstractSyntaxes = AbstractSyntax.ALL_RADIOTHERAPY_STORAGE;

					// Set the action when a DICOM file comes in and rename file so that name indicates type of DICOM file
					receiver.DIMSEService.CStoreService.CStorePayloadAction = (dcm, asc) =>
					{
						string path = "";

						if (dcm.GetSelector().Modality.Data == "RTPLAN")
						{
							path = Path.Combine(outpath, "RP." + dcm.GetSelector().SOPInstanceUID.Data + ".dcm");
						}
						else if (dcm.GetSelector().Modality.Data == "RTDOSE")
						{
							path = Path.Combine(outpath, "RD." + dcm.GetSelector().SOPInstanceUID.Data + ".dcm");
						}
						else if (dcm.GetSelector().Modality.Data == "RTSTRUCT")
						{
							path = Path.Combine(outpath, "RS." + dcm.GetSelector().SOPInstanceUID.Data + ".dcm");
						}
						else if (dcm.GetSelector().Modality.Data == "CT")
						{
							path = Path.Combine(outpath, "CT." + dcm.GetSelector().SOPInstanceUID.Data + ".dcm");
						}
						else
						{
							path = Path.Combine(outpath, dcm.GetSelector().SOPInstanceUID.Data + ".dcm");
						}

						Console.WriteLine($"Writing file {path}");
						dcm.Write(path);
						return true;
					};
					receiver.ListenForIncomingAssociations(true);

					Console.WriteLine(Environment.NewLine + Environment.NewLine + "********************************************************************************");
					Console.WriteLine("Starting Export...");

					// Create formatted date and time string to append to export log file name
					string s1 = DateTime.Now.ToString().Replace("/", "-");
					string formated_date_time = s1.Replace(":", "-");

					// Create export log file
					using (StreamWriter main_log = new StreamWriter(logPath + "\\DICOM_Export_Log " + formated_date_time + ".txt"))
					{
						main_log.WriteLine("--==DICOM EXPORT LOG==--" + Environment.NewLine);
						main_log.WriteLine("Start: " + DateTime.Now.ToString() + Environment.NewLine);

						// Print details of searches where patient/course/plan could not be identified to export log file
						if (missing_data_log.Count > 0)
						{
							main_log.WriteLine("**==Failed Searches==**");
						}

						for (int num = 0; num < missing_data_log.Count(); num++)
						{
							main_log.WriteLine(missing_data_log[num]);
						}

						main_log.WriteLine(Environment.NewLine + Environment.NewLine + Environment.NewLine + "**==Successful Searches==**");
						if (verified_patID.Count == 0) main_log.WriteLine("None");

						// For all rows of data input file where plan details were verified, export relevant DICOM files and print details of file exports to log file as well as console
						if (verified_patID.Count == planUIDs.Count && verified_patID.Count == structUIDs.Count && verified_patID.Count == doseUIDs.Count && verified_patID.Count == ctUIDs.Count)
						{
							// Loop through all patients with verified course/plan 
							for (int pt = 0; pt < verified_patID.Count(); pt++)
							{
								string patID = verified_patID[pt];
								string planUID = planUIDs[pt];
								string structUID = structUIDs[pt];
								string doseUID = doseUIDs[pt];
								string ctUID = ctUIDs[pt];

								string log = null;

								Console.WriteLine(Environment.NewLine + "--==" + patID + "==--");
								log += Environment.NewLine + patID + " :" + Environment.NewLine;

								// Build a finder class to help with C-FIND operations
								var finder = client.GetCFinder(daemon);
								var studies = finder.FindStudies(patID);
								var source = finder.FindSeries(studies);


								// Filter series by modality, then create list 
								var plans = source.Where(s => s.Modality == "RTPLAN").SelectMany(ser => finder.FindPlans(new List<CFindSeriesIOD>() { ser }));
								var doses = source.Where(s => s.Modality == "RTDOSE").SelectMany(ser => finder.FindDoses(new List<CFindSeriesIOD>() { ser }));
								var structures = source.Where(s => s.Modality == "RTSTRUCT").SelectMany(ser => finder.FindImages<CFindImageIOD>(ser));
								var cts = source.Where(s => s.Modality == "CT");

								CMover cMover = client.GetCMover(daemon);
								ushort msgID = 1;

								foreach (CFindInstanceIOD plan in plans)
								{
									// If plan UID matches the plan UID relevent to the current specified verified patient
									if (plan.SOPInstanceUID == planUID)
									{
										// Export plan file and write details to console and export log file
										Console.WriteLine(Environment.NewLine + "Exporting plan...");

										string log_temp = null;

										try
										{
											CMoveResponse response = cMover.SendCMove(plan, local.AeTitle, ref msgID);

											log_temp = Environment.NewLine + $"=== Plan Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : " + Environment.NewLine;
											log_temp += $"Number of Completed Operations : {response.NumberOfCompletedOps}" + Environment.NewLine;
											log_temp += $"Number of Failed Operations : {response.NumberOfFailedOps}" + Environment.NewLine;
											log_temp += $"Number of Remaining Operations : {response.NumberOfRemainingOps}" + Environment.NewLine;
											log_temp += $"Number of Warning Operations : {response.NumberOfWarningOps}" + Environment.NewLine;
										}
										catch (Exception exception)
										{
											log_temp = Environment.NewLine + $"=== Plan Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : EvilDICOM Crash" + Environment.NewLine;
											log_temp += $"***" + exception.Message + $"***" + Environment.NewLine;
										}

										log += log_temp;

										break;
									}
								}

								foreach (CFindInstanceIOD dose in doses)
								{
									// If dose UID matches the dose UID relevent to the current specified verified patient
									if (dose.SOPInstanceUID == doseUID)
									{
										// Export dose file and write details to console and export log file
										Console.WriteLine(Environment.NewLine + "Exporting dose...");

										string log_temp = null;

										try
										{
											CMoveResponse response = cMover.SendCMove(dose, local.AeTitle, ref msgID);

											log_temp = Environment.NewLine + $"=== Dose Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : " + Environment.NewLine;
											log_temp += $"Number of Completed Operations : {response.NumberOfCompletedOps}" + Environment.NewLine;
											log_temp += $"Number of Failed Operations : {response.NumberOfFailedOps}" + Environment.NewLine;
											log_temp += $"Number of Remaining Operations : {response.NumberOfRemainingOps}" + Environment.NewLine;
											log_temp += $"Number of Warning Operations : {response.NumberOfWarningOps}" + Environment.NewLine;
										}
										catch (Exception exception)
										{
											log_temp = Environment.NewLine + $"=== Dose Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : EvilDICOM Crash" + Environment.NewLine;
											log_temp += $"***" + exception.Message + $"***" + Environment.NewLine;
										}

										log += log_temp;

										break;
									}
								}

								foreach (CFindInstanceIOD structure in structures)
								{
									// If structure set UID matches the structure set UID relevent to the current specified verified patient
									if (structure.SOPInstanceUID == structUID)
									{
										// Export structure set file and write details to console and export log file
										Console.WriteLine(Environment.NewLine + "Exporting structure set...");

										string log_temp = null;

										try
										{
											CMoveResponse response = cMover.SendCMove(structure, local.AeTitle, ref msgID);

											log_temp = Environment.NewLine + $"=== Structure Set Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : " + Environment.NewLine;
											log_temp += $"Number of Completed Operations : {response.NumberOfCompletedOps}" + Environment.NewLine;
											log_temp += $"Number of Failed Operations : {response.NumberOfFailedOps}" + Environment.NewLine;
											log_temp += $"Number of Remaining Operations : {response.NumberOfRemainingOps}" + Environment.NewLine;
											log_temp += $"Number of Warning Operations : {response.NumberOfWarningOps}" + Environment.NewLine;
										}
										catch (Exception exception)
										{
											log_temp = Environment.NewLine + $"=== Structure Set Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : EvilDICOM Crash" + Environment.NewLine;
											log_temp += $"***" + exception.Message + $"***" + Environment.NewLine;
										}

										log += log_temp;

										break;
									}
								}

								foreach (CFindSeriesIOD ct in cts)
								{
									// If CT series UID matches the CT series UID relevent to the current specified verified patient
									if (ct.SeriesInstanceUID == ctUID)
									{
										// Export CT series and write details to console and export log file
										Console.WriteLine(Environment.NewLine + "Exporting images...");

										string log_temp = null;

										try
										{
											CMoveResponse response = cMover.SendCMove(ct, local.AeTitle, ref msgID);

											log_temp = Environment.NewLine + $"=== CT Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : " + Environment.NewLine;
											log_temp += $"Number of Completed Operations : {response.NumberOfCompletedOps}" + Environment.NewLine;
											log_temp += $"Number of Failed Operations : {response.NumberOfFailedOps}" + Environment.NewLine;
											log_temp += $"Number of Remaining Operations : {response.NumberOfRemainingOps}" + Environment.NewLine;
											log_temp += $"Number of Warning Operations : {response.NumberOfWarningOps}" + Environment.NewLine;
										}
										catch (Exception exception)
										{
											log_temp = Environment.NewLine + $"=== CT Export ===" + Environment.NewLine;
											log_temp += $"DICOM C-Move Results : EvilDICOM Crash" + Environment.NewLine;
											log_temp += $"***" + exception.Message + $"***" + Environment.NewLine;
										}

										log += log_temp;

										break;
									}
								}

								main_log.WriteLine(log);
							}
						}
						else
						{
							Console.WriteLine(Environment.NewLine + "ERROR::mismatch in data volumes: #patients(" + verified_patID.Count + "), #plans(" + planUIDs.Count + "), #structure sets(" + structUIDs.Count + "), #dose(" + doseUIDs.Count + "), #CT series(" + ctUIDs.Count + ")");
						}


						main_log.Close();
						receiver.Stop();

						Console.WriteLine(Environment.NewLine + "Export complete. " + verified_patID.Count + "/" + patIDs.Count + " plan exports successful. Please see export log file for details.");
						Console.WriteLine(Environment.NewLine + "********************************************************************************");
					
					} // end StreamWriter
				}
				else
				{
					Console.WriteLine(Environment.NewLine + "ERROR::Either not a single usable plan data set found in the input file or a mismatch in number of patient (" + patIDs.Count + "), course (" + courseIDs.Count + ") and/or plan (" + planIDs.Count + ") data extracted from the input file. Please check/correct input file and re-run script");
				}
			} // end while
		}  //end execute
	} // end class
} // end namespace