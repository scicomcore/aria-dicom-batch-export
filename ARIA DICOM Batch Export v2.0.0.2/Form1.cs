﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using VMS.TPS.Common.Model.API;

namespace DICOMExportForPROKNOW
{
	public partial class Form1 : Form
	{
		public VMS.TPS.Common.Model.API.Application app { get; set; }

		public string daemonTitle { get; set; }

		public string daemonIP { get; set; }

		public int daemonPort { get; set; }

		public string clientTitle { get; set; }

		public int clientPort { get; set; }

		public List<string> patientIDs { get; set; }

		public List<string> courseIDs { get; set; }

		public List<string> planIDs { get; set; }

		public string inpath { get; set; }

		public string outpath { get; set; }


		public Form1(VMS.TPS.Common.Model.API.Application app_1, string daemon_title, string daemon_ip, int daemon_port, string client_title, int client_port, string in_path, string out_path)
		{
			app = app_1;
			InitializeComponent();
			textBox1.Text = daemon_title;
			textBox2.Text = daemon_ip;
			textBox3.Text = daemon_port.ToString();
			textBox4.Text = client_title;
			textBox5.Text = client_port.ToString();
			textBox6.Text = in_path;
			textBox7.Text = out_path;
		}

		public void TextBox3_Text(object sender, EventArgs e)
		{
			try
			{
				Convert.ToInt32(textBox3.Text);
			}
			catch
			{
				MessageBox.Show("Please enter only integer values for the port number", "Invalid Port Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		public void TextBox5_Text(object sender, EventArgs e)
		{
			try
			{
				Convert.ToInt32(textBox5.Text);
			}
			catch
			{
				MessageBox.Show("Please enter only integer values for the port number", "Invalid Port Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		public void TextBox6_Text(object sender, EventArgs e)
		{
			try
			{
				if (File.Exists(textBox6.Text ?? "") && (textBox6.Text.Contains(".csv") || textBox6.Text.Contains(".txt")))
				{
					textBox6.BackColor = Color.PaleGreen;
				}
				else
				{
					textBox6.BackColor = Color.DarkOrange;
				}
			}
			catch
			{
				textBox6.BackColor = Color.DarkOrange;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.InitialDirectory = "C:\\";
			openFileDialog.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt";
			string text = null;
			if (Directory.Exists(textBox6.Text))
			{
				openFileDialog.InitialDirectory = textBox6.Text;
			}
			else
			{
				int num = textBox6.Text.Length - 1;
				while (num > 0 && textBox6.Text[num] != '\\')
				{
					text += textBox6.Text[num];
					num--;
				}
				text = ((text == null) ? textBox6.Text : textBox6.Text.TrimEnd(text.ToCharArray()));
				if (Directory.Exists(text))
				{
					openFileDialog.InitialDirectory = text;
				}
			}
			openFileDialog.Title = "Select the file Containing the Patient IDs, Course IDs and Plan IDs";
			openFileDialog.RestoreDirectory = true;
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				textBox6.Text = openFileDialog.FileName;
			}
			try
			{
				if (File.Exists(textBox6.Text ?? ""))
				{
					textBox6.BackColor = Color.PaleGreen;
				}
				else
				{
					textBox6.BackColor = Color.DarkOrange;
				}
			}
			catch
			{
				textBox6.BackColor = Color.DarkOrange;
			}
		}

		public void TextBox7_Text(object sender, EventArgs e)
		{
			try
			{
				if (Directory.Exists(textBox6.Text))
				{
					textBox7.BackColor = Color.PaleGreen;
				}
				else
				{
					textBox7.BackColor = Color.DarkOrange;
				}
			}
			catch
			{
				textBox7.BackColor = Color.DarkOrange;
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.ShowNewFolderButton = true;
			string text = null;
			if (Directory.Exists(textBox7.Text))
			{
				folderBrowserDialog.SelectedPath = textBox7.Text;
			}
			else
			{
				int num = textBox7.Text.Length - 1;
				while (num > 0 && textBox7.Text[num] != '\\')
				{
					text += textBox7.Text[num];
					num--;
				}
				text = ((text == null) ? textBox7.Text : textBox7.Text.TrimEnd(text.ToCharArray()));
				if (Directory.Exists(text))
				{
					folderBrowserDialog.SelectedPath = text;
				}
				else
				{
					folderBrowserDialog.SelectedPath = "C:\\";
				}
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				textBox7.Text = folderBrowserDialog.SelectedPath;
			}
			text = textBox7.Text;
			try
			{
				if (Directory.Exists(textBox7.Text ?? ""))
				{
					textBox7.BackColor = Color.PaleGreen;
				}
				else
				{
					textBox7.BackColor = Color.DarkOrange;
				}
			}
			catch
			{
				textBox7.BackColor = Color.DarkOrange;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (textBox6.BackColor == Color.PaleGreen && textBox7.BackColor == Color.PaleGreen)
			{
				daemonTitle = textBox1.Text;
				daemonIP = textBox2.Text;
				daemonPort = Convert.ToInt32(textBox3.Text);
				clientTitle = textBox4.Text;
				clientPort = Convert.ToInt32(textBox5.Text);
				List<string> list = new List<string>();
				List<string> list2 = new List<string>();
				List<string> list3 = new List<string>();

				// Unpack data input file
				string[] array = File.ReadAllLines(textBox6.Text);
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i].Split(',').Length >= 3)
					{
						list.Add(array[i].Split(',')[0]);
						list2.Add(array[i].Split(',')[1]);
						list3.Add(array[i].Split(',')[2]);
					}
				}
				patientIDs = list;
				courseIDs = list2;
				planIDs = list3;
				inpath = textBox6.Text;
				outpath = textBox7.Text;
				Close();
			}
			else if (textBox6.BackColor == Color.PaleGreen)
			{
				MessageBox.Show("Please select a valid destination folder for the exported DICOM files", "Invalid File Path", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else if (textBox7.BackColor == Color.PaleGreen)
			{
				MessageBox.Show("Please select a valid patient information file for input", "Invalid File Path", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else
			{
				MessageBox.Show("Please select valid file paths for input and output", "Invalid File Path", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			button3.Text = "Export";
			button3.Refresh();
		}
	}
}
