# DICOMExportForPROKNOW #

### What is this repository for? ###

This C# project code has been designed specifically to enable UK NHS centres to batch export treatment DICOM data from ARIA to ProKnow.
Instructions for the implementation of the program will be provided when requested through official channels.

Version: v2.0.0.2

### How do I get set up? ###

Instructions for the implementation of the program will be provided when requested through official channels.

### Contribution guidelines ###

Instructions for contributing will be provided when requested through official channels.

### Who do I talk to? ###

This code was developed at the Royal Surrey NHS Foundation Trust in 2022.
